import { tap } from 'rxjs/operators';
import { isLoggedIn } from './auth.selectors';
import { Store, select } from '@ngrx/store';
import { AppState } from './../reducers/index';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';



@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private store: Store<AppState>,
        private router: Router
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.store.pipe(
            select(isLoggedIn),
            tap(isLoggedIn => {
                if (!isLoggedIn) { this.router.navigateByUrl('/login') }
            })
        );
    }

}