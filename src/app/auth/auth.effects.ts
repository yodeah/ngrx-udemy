import { of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthActions, AuthActionTypes, Login, Logout } from './auth.actions';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { ofType } from '@ngrx/effects';
import { defer } from 'rxjs';


@Injectable()
export class AuthEffects {

  @Effect({"dispatch":false})
  login$ = this.actions$.pipe(
    ofType<Login>(AuthActionTypes.LoginAction),
    tap( loginAction => localStorage.setItem("user",JSON.stringify(loginAction.payload)))
  );

  @Effect({"dispatch":false})
  logout$ = this.actions$.pipe(
    ofType<Logout>(AuthActionTypes.LogoutAction),
    tap( logoutAction => {
      localStorage.removeItem("user");
      this.router.navigateByUrl("/login");
    }));

  @Effect()
  init$ = defer(() => {
    const userData = localStorage.getItem("user");

    if(userData){
      return of(new Login(JSON.parse(userData)));
    }else{
      return of(new Logout());
    }
  });  

  constructor(private actions$: Actions, private router:Router) {}
}
