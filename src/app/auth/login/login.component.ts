import { User } from './../../model/user.model';
import { Login } from './../auth.actions';
import { AppState } from './../../reducers/index';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import {Store} from "@ngrx/store";

import {AuthService} from "../auth.service";
import {tap} from "rxjs/operators";
import {noop} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;

  constructor(
      private fb:FormBuilder,
      private auth: AuthService,
      private router:Router,
      private store:Store<AppState>
    ) {

      this.form = fb.group({
          email: ['test@angular-university.io', [Validators.required]],
          password: ['test', [Validators.required]]
      });

  }

  ngOnInit() {

  }

  login() {
    this.auth.login(
      this.form.value.email, 
      this.form.value.password
    ).pipe(
      tap(  (v:User) => {
        console.log("tapping v:", v);
        this.store.dispatch(new Login(v));
        this.router.navigate(["/courses"]);
      })
    ).subscribe(
      noop,
      () => {alert("error while logging in")}
    );
  }


}
