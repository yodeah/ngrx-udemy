import { User } from './../model/user.model';
import { AuthActionTypes, AuthActions } from './auth.actions';
import { Action } from '@ngrx/store';



export interface AuthState {
  loggedIn:boolean;
  user:User;
}

export const initialAuthState:AuthState = {
  loggedIn:false,
  user:undefined
};

export function reducer(state = initialAuthState, action: AuthActions): AuthState {
  switch(action.type) {
    case AuthActionTypes.LoginAction: 
      return {
        loggedIn: true,
        user: action.payload
      };
    case  AuthActionTypes.LogoutAction: 
      return {
        loggedIn: false,
        user: undefined
      }
    default: 
      return state;
  }
}
