import { CoursesState } from "./course.reducers";
import { createFeatureSelector, createSelector } from "@ngrx/store";

export const selectCoursesState =   createFeatureSelector<CoursesState>("courses");

export const selectCourseById = (courseId:number) => createSelector(
    selectCoursesState,
    coursesState => coursesState.entities[courseId]
) 