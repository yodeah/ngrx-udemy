import { Course } from "./model/course";

export interface CoursesState{

    coursesEntities: {[key:number]: Course};

    coursesOrder: number[];

}

export interface LessonsState{

    coursesEntities: {[key:number]: Course};

    coursesOrder: number[];

}