import { selectCourseById } from './../course.selectors';



import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Course} from "../model/course";
import {Observable} from "rxjs";
import {CoursesService} from "./courses.service";
import { Store, select } from "@ngrx/store";
import { AppState } from "../../reducers";
import { CourseRequested } from '../course.action';
import { tap, filter, first } from 'rxjs/operators';



@Injectable()
export class CourseResolver implements Resolve<Course> {

    constructor(private coursesService:CoursesService, private store: Store<AppState>) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Course> {
        //return this.coursesService.findCourseById(route.params['id']);

        const courseId = route.params['id'];
        return this.store.pipe(
            select(selectCourseById(courseId)),
            tap(course => {
                if(!course){
                    this.store.dispatch(new CourseRequested({courseId}));
                }
            }),
            filter(course => !!course),
            first()
        )

    }

}

